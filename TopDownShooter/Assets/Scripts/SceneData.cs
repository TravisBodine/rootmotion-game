﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneData : MonoBehaviour
{
    public string BGM;
    // Start is called before the first frame update
    void Start()
    {
        AudioManager.instance.Play(BGM);
    }

    private void OnDestroy()
    {
        AudioManager.instance.Stop(BGM);
    }
}

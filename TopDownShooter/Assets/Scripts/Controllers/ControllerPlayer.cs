﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerInput))]
public class ControllerPlayer : Controller
{
    PlayerInput input;

    PawnPlayer playerPawn;

    

    // Start is called before the first frame update
    void Start()
    {
        //GameManager.instance.player = this;
        input = GetComponent<PlayerInput>();

        input.OnFireButtonHeld += Attack;
        input.OnReloadButtonPressed += Reload;

        //make sure the players pawn is of type player pawn
        if (pawn.GetType() == typeof(PawnPlayer))
        {
            playerPawn = (PawnPlayer)pawn;
        }
        else
        {
            Debug.LogError("Players pawn is not a player pawn");
        }

       
        pawn.health.onDie += Die;

        GameManager.instance.player = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.isPaused)
        {
            return;
        }

        if (!pawn.isDead)
        {
            if (input.sprintButtonHeld)
            {
                // playerPawn.playerIK.weight = 0;
                playerPawn.Sprint(input.HorizontalAxis, input.VerticalAxis);
            }
            else
            {
                // playerPawn.playerIK.ResetWeight();
                playerPawn.Walk(input.HorizontalAxis, input.VerticalAxis);
            }

            playerPawn.LookAtMousePosition();
        }
 

    }

    void Attack()
    {
        if (GameManager.instance.isPaused)
        {
            return;
        }
        if (!input.sprintButtonHeld && !pawn.isDead)
        {
            playerPawn.Attack();
        }  
    }

    void Reload()
    {
        if (GameManager.instance.isPaused)
        {
            return;
        }

        playerPawn.Reload();
    }

    protected override void Die()
    {
        EventSystem.TriggerEvent("PlayerDied", this);
        AudioManager.instance.Play("GameOver");
        pawn.Die();
    }

}

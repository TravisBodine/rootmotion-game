﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class ControllerAi : Controller
{
    [SerializeField] float respawnTime;
    PawnAI pawnAI;
    enum AIControllerStates
    {
        idle,
        chase,
        attack,
        dead
    }
    AIControllerStates currentState;
    private void Start()
    {
        pawnAI = pawn as PawnAI;
        currentState = AIControllerStates.chase;
        pawn.health.onDie += Die;
    }
    protected void Update()
    {

        if (GameManager.instance.isPaused)
        {
            return;
        }

        switch (currentState)
        {
            case AIControllerStates.idle:
                break;
            case AIControllerStates.chase:
                Chase();
                break;
            case AIControllerStates.attack:
                Attack();
                break;
            case AIControllerStates.dead:
                break;
        }
    }

    void Chase()
    {
        Vector3 target = GameManager.instance.player.transform.position;
        pawnAI.MoveToTarget(target);


        if ((target - pawnAI.transform.position).magnitude <= pawnAI.navMeshAgent.stoppingDistance)
        {
            currentState = AIControllerStates.attack;
        }
       
    }

    void Attack()
    {
        pawn.Attack();

        Vector3 target = GameManager.instance.player.transform.position;
        if ((target - pawnAI.transform.position).magnitude > pawnAI.navMeshAgent.stoppingDistance)
        {
            currentState = AIControllerStates.chase;
        }
    }
    protected override void Die()
    {
        currentState = AIControllerStates.dead;
        GameManager.instance.DeadEnemies.Add(this);
        GameManager.instance.AliveEnemies.Remove(this);
        pawn.Die();
        //StartCoroutine(WaitForRespawn());
    }

    //IEnumerator WaitForRespawn()
    //{
    //    yield return new WaitForSeconds(respawnTime);
    //    Respawn();
    //}

    //void Respawn()
    //{
    //    currentState = AIControllerStates.chase;
    //    pawn.health.FullHeal();
    //    pawn.Respawn();
    //}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerInput : MonoBehaviour
{
    public float HorizontalAxis { get; private set; }

    public float VerticalAxis { get; private set; }

    public event Action OnFireButtonPressed = delegate { };
    public event Action OnFireButtonHeld = delegate { };

    public event Action OnReloadButtonPressed = delegate { };

    public bool sprintButtonHeld = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");
        VerticalAxis = Input.GetAxis("Vertical");

       
        if (Input.GetMouseButtonDown(0))
        {
            OnFireButtonPressed();
        }
        if (Input.GetMouseButton(0))
        {
            OnFireButtonHeld();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            OnReloadButtonPressed();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            sprintButtonHeld = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            sprintButtonHeld = false;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.instance.TogglePause(!GameManager.instance.isPaused);
        }
    }
}

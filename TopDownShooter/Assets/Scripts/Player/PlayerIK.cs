﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Pawn))]
public class PlayerIK : MonoBehaviour
{
    //put these on the weapon
    public Transform rhpoint;
    public Transform lhpoint;

    public Transform rEpoint;
    public Transform lEpoint;

    [Range(0, 1)]
    public float weight;

    float _weight;

    Animator anim;
    Pawn pawn;

    bool hasHandPoints = true;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        pawn = GetComponent<Pawn>();
        if (pawn.currentWeapon != null)
        {
            rhpoint = pawn.currentWeapon.RightHandPoint;
            lhpoint = pawn.currentWeapon.LeftHandPoint;
            hasHandPoints = true;
        }
        else
        {
            hasHandPoints = false;
        }
        _weight = weight;
    }

    public void SetHandPoints(Transform RightHandPoint,Transform LeftHandPoint)
    {
        rhpoint = RightHandPoint;
        lhpoint = LeftHandPoint;
        hasHandPoints = true;
    }

    public void ResetWeight()
    {
        weight = _weight;
    }

    public void OnAnimatorIK()
    {
        if (hasHandPoints)
        {
            anim.SetIKPosition(AvatarIKGoal.LeftHand, lhpoint.position);

            anim.SetIKRotation(AvatarIKGoal.LeftHand, lhpoint.rotation);

            anim.SetIKPosition(AvatarIKGoal.RightHand, rhpoint.position);
            anim.SetIKRotation(AvatarIKGoal.RightHand, rhpoint.rotation);

            anim.SetIKHintPosition(AvatarIKHint.RightElbow, rEpoint.position);
            anim.SetIKHintPosition(AvatarIKHint.LeftElbow, lEpoint.position);
            anim.SetIKHintPositionWeight(AvatarIKHint.RightElbow, weight);
            anim.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, weight);

            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, weight);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, weight);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, weight);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, weight); 
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnPlayer : Pawn
{
    public float sprintSpeed;

    public PlayerIK playerIK;

    public float minCameraLookDistance;
    Camera mouseCam;

    bool reloading = false;

    public override void Start()
    {
        base.Start();
        playerIK = GetComponent<PlayerIK>();
        mouseCam = Camera.main;
    }

    public override void Attack()
    {
        if (!reloading)
        {
            currentWeapon.Attack();
        }
        
    }

    public void Reload()
    {
        Gun gun = currentWeapon as Gun;
        if( gun != null && !reloading)
        {
            if (!gun.isFull)
            {

                anim.SetBool("Reloading", true);
                //playerIK.weight = 0;
                reloading = true;
                StartCoroutine(WaitForReload(gun, gun.reloadTime));
                UIHUD.instance.reload.StartReloadCounter(gun.reloadTime);
            }
       
        }
    }

    IEnumerator WaitForReload(Gun gun ,float time)
    {
        yield return new WaitForSeconds(time);
        reloading = false;
        gun.Reload();
        anim.SetBool("Reloading", false);
        //playerIK.weight = 1;
        UIHUD.instance.UpdateUI();
    }


    //TODO: pool
    public void ChangeWeapon(Weapon weaponToChangeTo)
    {
        Weapon newWep = Instantiate(weaponToChangeTo, weaponHolder.transform.position, weaponHolder.transform.rotation);
      
        newWep.gameObject.transform.parent = weaponHolder.transform;
        playerIK.SetHandPoints(newWep.RightHandPoint, newWep.LeftHandPoint);
        Destroy(currentWeapon.gameObject);

        // i think im giving up on having meele weapons
        Gun gun = newWep as Gun;
        gun.Reload();

        currentWeapon = newWep;
        
      
        UIHUD.instance.UpdateUI();
    }

    public override void LookAt(Vector3 pointToLookAt)
    {
        
    }

    public void Sprint(float HorzAmount, float VertAmount)
    {
        movement = new Vector3(HorzAmount, 0, VertAmount).normalized;

        movement = transform.InverseTransformDirection(movement);

        anim.SetFloat("Horizontal", (movement).x * sprintSpeed);
        anim.SetFloat("Vertical", (movement).z * sprintSpeed);
    }

    public void LookAtMousePosition()
    {
        //plane at player position
        Plane groundPlane = new Plane(Vector3.up, transform.position);

        //get a ray where the mouse is pointing and get where that intersects the plane
        float rayDistToIntersect;
        Ray ray = mouseCam.ScreenPointToRay(Input.mousePosition);
        if (groundPlane.Raycast(ray, out rayDistToIntersect))
        {
            //get point from how far down the ray it is
            Vector3 collisonPoint = ray.GetPoint(rayDistToIntersect);

            //make the point a minimum distance away from the player to reduce jitteryness
            //Vector3 MinDistanceAwayLookPoint = transform.position+ ((collisonPoint- transform.position).normalized * minCameraLookDistance);

            //only look at it if its a certain distance away to avoid jitter
            if (Vector3.Distance(transform.position, collisonPoint) > minCameraLookDistance)
            {
                //instant look
                transform.LookAt(collisonPoint);
            }

            //slow rotate
            // Quaternion targetRotation = Quaternion.LookRotation(collisonPoint - transform.position,Vector3.up);
            //  transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
        }
        else
        {
            Debug.LogError("Camera is not looking at plane");
        }
    }

    public override void Die()
    {
        base.Die();
        ragdollControls.ActivateRagdoll();
    }

    public override void Respawn()
    {
        throw new System.NotImplementedException();
    }
}

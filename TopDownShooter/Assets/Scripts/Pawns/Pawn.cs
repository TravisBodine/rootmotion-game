﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public abstract class Pawn : MonoBehaviour
{
    [SerializeField] protected float speed;

    [SerializeField] protected float turnSpeed;

    [SerializeField] protected Transform weaponHolder;
    [SerializeField] public Weapon currentWeapon;

    protected Animator anim;
    protected Rigidbody rb;
    protected RagdollControls ragdollControls;

    protected Vector3 movement;

    public Health health;

    public bool isDead = false;

    public virtual void Start()
    {
        anim = GetComponent<Animator>();
        health = GetComponent<Health>();
        rb = GetComponent<Rigidbody>();
        ragdollControls = GetComponent<RagdollControls>();
    }
    public virtual void Walk(float HorzAmount, float VertAmount)
    {
        movement = new Vector3(HorzAmount, 0, VertAmount).normalized;

        movement = transform.InverseTransformDirection(movement);

        anim.SetFloat("Horizontal", (movement).x * speed);
        anim.SetFloat("Vertical", (movement).z * speed);
    }
    public abstract void Attack();
    public abstract void LookAt(Vector3 pointToLookAt);
    public virtual void Die()
    {
        isDead = true;
    }
    public abstract void Respawn();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PawnAI : Pawn
{
    public NavMeshAgent navMeshAgent;

    Vector3 input;

    [SerializeField] WeightedDropTable drops;

    public override void Start()
    {
        
        base.Start();
        drops = GetComponent<WeightedDropTable>();
        navMeshAgent = GetComponent<NavMeshAgent>();

        //navMeshAgent.updatePosition = false;
        //navMeshAgent.updateRotation = false;
    }

    public override void Attack()
    {
        currentWeapon.Attack();
    }

    public override void LookAt(Vector3 pointToLookAt)
    {
        transform.LookAt(pointToLookAt);
    }
    public void MoveToTarget(Vector3 target)
    {
        //i think i might need to get more animations / set the move ment to use vertical horizontal like the player so that i can take the desired velocity
        navMeshAgent.destination = target;

        //this is from corse shell
        input = navMeshAgent.desiredVelocity;
        input = transform.InverseTransformDirection(input);

        //animator.SetFloat("Horizontal", input.x);
        //animator.SetFloat("Vertical", input.z);
        //Debug.Log(transform.position + input.normalized);
        //LookAt(navMeshAgent.nextPosition);
        //navMeshAgent.isStopped = true;

        anim.SetFloat("Speed", speed);

    }

    
    private void OnAnimatorMove()
    {
        //        Debug.Log(navMeshAgent.velocity);

        if (navMeshAgent.desiredVelocity != Vector3.zero)
        {
            navMeshAgent.velocity = navMeshAgent.desiredVelocity.normalized * anim.velocity.magnitude;
        }

    }

    public void Reload()
    {
        Gun gun = currentWeapon as Gun;
        if (gun != null)
        {
            anim.SetBool("Reloading", true);
            gun.Reload();
            UIHUD.instance.UpdateUI();
        }
    }

    public override void Die()
    {
        base.Die();

        rb.velocity = Vector3.zero;
        navMeshAgent.destination = transform.position;
        ragdollControls.ActivateRagdoll();

        PowerUp powerUp = drops.DropRandomItem();
        if(powerUp != null)
        {
            Instantiate(powerUp, transform.position, Quaternion.identity);
        }

        AudioManager.instance.Play("EnemyDied");
        EventSystem.TriggerEvent("EnemyDied", this);

    }

    public override void Respawn()
    {
        ragdollControls.DeactivateRagdoll();
    }
}

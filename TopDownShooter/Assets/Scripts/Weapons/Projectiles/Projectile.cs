﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    
    public float timeOutTime;
    [HideInInspector]
    public int damage;
    [HideInInspector]
    public int speed;
    [HideInInspector]
    public Vector3 Direction = new Vector3(0, 0, 0);

    private void Start()
    {
        //transform.rotation = Quaternion.LookRotation(Direction);
        Destroy(gameObject, timeOutTime);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Direction * speed * Time.deltaTime;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gun : Weapon
{
    [SerializeField] protected int ammoCapacity;
    protected int currentAmmo;
    public int CurrentAmmo
    {
        get
        {
            return currentAmmo;
        }
    }
    [SerializeField] protected float range;
    public float reloadTime;
    public bool isFull {
        get
        {
            return CurrentAmmo == ammoCapacity;
        }
       
    }

    [SerializeField] protected Transform firePoint;
    [SerializeField] protected BulletLineRenderer lineRenderer;
    [SerializeField] protected float lineDespawnTime;
    [SerializeField] protected Projectile projectile;
    [SerializeField] protected int projectileVelocity;

    [SerializeField] protected ParticleSystem fireParticle;

    [SerializeField] protected string gunShotSound;

    public Sprite ammoSprite;
    public Sprite gunSprite;

    protected float timer = 0;
    protected virtual void Start()
    {
        currentAmmo = ammoCapacity;
    }

    /// <summary>
    /// checks the ammo and firerate and fires the weapon 
    /// </summary>
    public virtual void Fire()
    {
        if (currentAmmo <= 0)
        {
            return;
        }

        if (timer > Time.time)
        {
            return;
        }
        else
        {
            timer = Time.time + ROF;
        }

        AudioManager.instance.Play(gunShotSound);
        //InstatiatePojectile();
        ShootRaycast();
        PlayEffects();
    }

    ////switching to raycasts
    //protected virtual void InstatiatePojectile()
    //{
    //    Projectile newProjectile = Instantiate(projectile, firePoint.position, Quaternion.identity);
    //    newProjectile.speed = projectileVelocity;
    //    newProjectile.damage = damage;
    //    newProjectile.Direction = firePoint.forward;
    //    currentAmmo--;
    //}

    protected virtual void ShootRaycast()
    {

        Ray ray = new Ray(firePoint.position, firePoint.transform.forward);

        RaycastHit hit;
        Physics.Raycast(ray, out hit, range);

        if (hit.collider != null)
        {
            Health hitHealth = hit.collider.gameObject.GetComponent<Health>();
            hitHealth?.TakeDamage(damage);
        }
        //fireParticle.Play();
        AddLineRenderer(hit, firePoint.transform.forward);

        currentAmmo--;

        UIHUD.instance.UpdateUI();

    }
    protected virtual void PlayEffects()
    {
        fireParticle?.Play();
    }
    protected virtual void AddLineRenderer(RaycastHit hit, Vector3 direction)
    {
        BulletLineRenderer bulletLine = Instantiate(lineRenderer);
        lineRenderer.fadeTime = lineDespawnTime;
        bulletLine.GetComponent<LineRenderer>().SetPosition(0, firePoint.position);
        if (hit.point != Vector3.zero)
        {
            bulletLine.GetComponent<LineRenderer>().SetPosition(1, hit.point);
        }
        else
        {
            bulletLine.GetComponent<LineRenderer>().SetPosition(1, firePoint.position + direction * range);
        }
    }


    /// <summary>
    /// resets the guns ammo to max
    /// </summary>
    public virtual void Reload()
    {
        //Debug.Log("reload");
        currentAmmo = ammoCapacity;
    }

    public override void Attack()
    {
        Fire();
    }
}

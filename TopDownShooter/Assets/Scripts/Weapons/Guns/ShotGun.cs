﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGun : Gun
{
    [SerializeField] int numOfPellets;
    [SerializeField] [Range (10,180)] float angle;

    public override void Fire()
    {
        base.Fire();
       
    }
    protected override void ShootRaycast()
    { 
        float halfAngle = angle / 2;

        for (int i = 0; i < numOfPellets; i++)
        {

            float randomDir = Random.Range(-halfAngle, halfAngle);
            Quaternion bulletAngle = Quaternion.Euler(0, randomDir, 0 );

            Vector3 direction = (bulletAngle * firePoint.transform.forward).normalized;
            Ray ray = new Ray(firePoint.position, direction);

            RaycastHit hit;
            Physics.Raycast(ray, out hit,range);

            
            if (hit.collider != null)
            {
                Health hitHealth = hit.collider.gameObject.GetComponent<Health>();
                hitHealth?.TakeDamage(damage);
            }

            AddLineRenderer(hit, direction);
        }

        currentAmmo--;

        UIHUD.instance.UpdateUI();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField] protected int damage;
    [SerializeField] protected float ROF;

    public Transform RightHandPoint;
    public Transform LeftHandPoint;

    public abstract void Attack();

}

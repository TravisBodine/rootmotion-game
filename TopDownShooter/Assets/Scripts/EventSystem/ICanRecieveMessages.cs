﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICanRecieveMessages
{
   void ReceiveMessage<T>(string eventType,T genericType);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public delegate void OnTakeDamage();
    public OnTakeDamage onTakeDamage = delegate { };
    public delegate void OnHeal();
    public OnHeal onHeal = delegate { };

    public delegate void OnHealthChanged();
    public OnHealthChanged onHealthChanged = delegate { };

    public delegate void OnDie();
    public OnTakeDamage onDie = delegate { };

    public float maxHealth;

    //public for testing purposues
    public float _health;

    public float health{ get
        {
            return _health;
        }
        private set
        {
            _health = value;

            EventSystem.TriggerEvent("HealthChanged", _health);
            onHealthChanged();
        }
    }

    public float healthPercent
    {
        get
        {
          return health / maxHealth;
        }
        //set
        //{
        //    health = value * maxHealth;
        //}
    }

    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
    }

    public void TakeDamage(float amount)
    {
        if (amount >= 0)
        {
            AudioManager.instance.Play("EnemyHit");
            onTakeDamage();
            health -= amount;
            CheckForDeath();
        }
    }

    public void CheckForDeath()
    {
        if (health <= 0)
        {
            onDie();
        }
    }

    public void Heal(float amount)
    {
        if (amount >= 0)
        {
            onHeal();
            health += amount;
            health = Mathf.Clamp(health,0, maxHealth);

        }
    }
    public void FullHeal()
    {
        onHeal();
        health = maxHealth;
    }
       
  

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPowerUp : PowerUp
{
    [SerializeField] Weapon weapon;

    protected override void PickedUp(Pawn pawn)
    {
        if(pawn.GetType() == typeof(PawnPlayer))
        {
            PawnPlayer player = (PawnPlayer)pawn;
            player.ChangeWeapon(weapon);
            base.PickedUp(pawn);
        }
        
    }
}

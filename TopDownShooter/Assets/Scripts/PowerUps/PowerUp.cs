﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public GameObject Mesh;

    bool active = true;

    [SerializeField] float respawnTime;

    protected virtual void Start()
    {
       
    }

    protected virtual void PickedUp(Pawn pawn)
    {
        AudioManager.instance.Play("PowerUp");
        Mesh.gameObject.SetActive(false);
        active = false;
        StartCoroutine(WaitForRespawn());
    }

    protected virtual IEnumerator WaitForRespawn()
    {
        yield return new WaitForSeconds(respawnTime);
        Respawn();
    }

    protected virtual void Respawn()
    {
        Mesh.gameObject.SetActive(true);
        active = true;
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (active)
        {
            Pawn pawn = other.gameObject.GetComponent<Pawn>();

            if (pawn != null)
            {
                PickedUp(pawn);
            }
        }
    }
}

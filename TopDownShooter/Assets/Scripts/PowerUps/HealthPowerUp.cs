﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPowerUp : PowerUp
{
    [SerializeField] float amountRestored;

    protected override void PickedUp(Pawn pawn)
    {
        pawn.health.Heal(amountRestored);
        base.PickedUp(pawn);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHealth : UIObject
{
    public Image HealthBar;
    /// <summary>
    /// cant seem to get it set on start correctly using this for now
    /// </summary>
    public ControllerPlayer player;
    public override void UpdateUI()
    {

        if(GameManager.instance.player != null)
        {
            if(HealthBar != null)
            {
                HealthBar.fillAmount = GameManager.instance.player.pawn.health.healthPercent;
            }
            
        }
        
        //player.pawn.health.healthPercent;//
    }
}

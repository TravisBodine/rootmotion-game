﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour
{
    [SerializeField] PawnAI owner;
    Image healthBar;
    // Start is called before the first frame update
    void Start()
    {
        healthBar = GetComponent<Image>();
        owner.health.onHealthChanged += UpdateHealthBar;
    }

    // Update is called once per frame
    void UpdateHealthBar()
    {

        healthBar.fillAmount = owner.health.healthPercent;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResolutionDropdown : MonoBehaviour
{
    public Resolution resolution;
    TMP_Dropdown dropdown;
    Dictionary<string, Resolution> resolutions = new Dictionary<string, Resolution>();

    // Start is called before the first frame update
    void Start()
    {
        //resolution = Screen.currentResolution;

        dropdown = GetComponent<TMP_Dropdown>();
        dropdown.ClearOptions();
        List<string> options = new List<string>();
        int index = 0;
        int currentRes = 0;
        foreach (var res in Screen.resolutions)
        {

            string option = string.Format("{0} x {1}", res.width, res.height);

            if ((res.width == Screen.currentResolution.width) && (res.height == Screen.currentResolution.height) && (res.refreshRate == Screen.currentResolution.refreshRate))
            {
                Debug.Log(index);
                currentRes = index;
            }

            options.Add(option);
            resolutions.Add(option, res);
            index++;
        }


        dropdown.AddOptions(options);
        dropdown.value = currentRes;
        dropdown.onValueChanged.AddListener(delegate { OnValueChanged(); });
       
    }

    public void OnValueChanged()
    {
        resolution = resolutions[dropdown.options[dropdown.value].text];
        
        
    }
}

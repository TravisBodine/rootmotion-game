﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QualityDropDown : MonoBehaviour
{
    public int qualitySetting;
    TMP_Dropdown dropdown;
    Dictionary<string, int> quality = new Dictionary<string, int>();

    // Start is called before the first frame update
    void Start()
    {
        qualitySetting = QualitySettings.GetQualityLevel();
        dropdown = GetComponent<TMP_Dropdown>();
        dropdown.ClearOptions();
        List<string> options = new List<string>();

        foreach (var name in QualitySettings.names)
        {

            options.Add(name);
            quality.Add(name, QualitySettings.names.Rank);
        }

        dropdown.AddOptions(options);
        dropdown.onValueChanged.AddListener(delegate { OnValueChanged(); });

        dropdown.value = QualitySettings.GetQualityLevel();
    }

    public void OnValueChanged()
    {
       qualitySetting = quality[dropdown.options[dropdown.value].text];


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{

    public ResolutionDropdown resolutionDropdown;
    public FullScreenToggle fullScreenToggle;
    public QualityDropDown qualitySettings;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ApplyChanges()
    {
        QualitySettings.SetQualityLevel(qualitySettings.qualitySetting);
        bool fullScreen = fullScreenToggle.fullScreen;
        
        Resolution res = resolutionDropdown.resolution;
      
        Screen.SetResolution(res.width, res.height, fullScreen);

        Debug.Log("full screen");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour,ICanRecieveMessages
{

    // Start is called before the first frame update
    void Start()
    {

        EventSystem.SubscribeToEvent("GamePaused", this);
        
        gameObject.SetActive(false);
    }

    public void ReceiveMessage<T>(string eventType, T genericType)
    {
        if (GameManager.instance.isPaused)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
        
    }
    private void OnDestroy()
    {
        EventSystem.UnsubscribeToEvent("GamePaused", this);
    }
}

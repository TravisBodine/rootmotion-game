﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScreen : MonoBehaviour, ICanRecieveMessages
{
  
    // Start is called before the first frame update
    void Start()
    {
        
        EventSystem.SubscribeToEvent("GameWon", this);
        gameObject.SetActive(false);
    }

    public void ReceiveMessage<T>(string eventType, T genericType)
    {
        gameObject.SetActive(true);
       
    }
    private void OnDestroy()
    {
        EventSystem.UnsubscribeToEvent("GameWon", this);
    }
}

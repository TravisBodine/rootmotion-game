﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UITest : MonoBehaviour
{
    public Dropdown dropdown;
    // Start is called before the first frame update
    void Start()
    {
        dropdown.ClearOptions();
        List<string> options = new List<string>();

        //just use a dictionary?

        foreach (var res in Screen.resolutions)
        {
            Debug.Log(res);
            options.Add(res.ToString());
        }

        dropdown.AddOptions(options);
        dropdown.onValueChanged.AddListener(delegate { OnValueChanged(); });
    }

    public void OnValueChanged()
    {

    }


    // Update is called once per frame
    void Update()
    {
        
    }
}

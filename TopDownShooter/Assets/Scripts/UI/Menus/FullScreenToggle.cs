﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]

public class FullScreenToggle : MonoBehaviour
{
    public bool fullScreen;
    Toggle toggle;

    // Start is called before the first frame update
    void Start()
    {
        fullScreen = Screen.fullScreen;

        toggle = GetComponent<Toggle>();
        toggle.isOn = Screen.fullScreen;
        toggle.onValueChanged.AddListener(delegate { ChangeFullScreen(); });

    }

    void ChangeFullScreen()
    {

        fullScreen = toggle.isOn;
    }

}

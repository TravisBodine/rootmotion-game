﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DeathScreen : MonoBehaviour,ICanRecieveMessages
{

    [SerializeField] TextMeshProUGUI roundsSurvived;
    // Start is called before the first frame update
    void Start()
    {
        //roundsSurvived = GetComponent<TextMeshProUGUI>();
        EventSystem.SubscribeToEvent("PlayerDied", this);
        gameObject.SetActive(false);
    }

    public void ReceiveMessage<T>(string eventType, T genericType)
    {
        gameObject.SetActive(true);
        roundsSurvived.text = (GameManager.instance.enemySpawner.Round-1).ToString();
    }
    private void OnDestroy()
    {
        EventSystem.UnsubscribeToEvent("PlayerDied", this);
    }
}

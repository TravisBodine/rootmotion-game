﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentWeaponUI : UIObject
{
    Image image;

 

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
    }

   
    public override void UpdateUI()
    {

        if(GameManager.instance.player == null)
        {
            return;
        }
        
        Gun gun = GameManager.instance.player.pawn.currentWeapon as Gun;
        if( gun != null)
        {
            image.sprite = gun.gunSprite;
        }
       
            
    }
}

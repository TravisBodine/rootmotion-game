﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
public class UIRoundCounter : MonoBehaviour,ICanRecieveMessages
{
    TextMeshProUGUI rounds;
    // Start is called before the first frame update
    void Start()
    {
        rounds = GetComponent<TextMeshProUGUI>();
        EventSystem.SubscribeToEvent("RoundIncreased", this);
    }


    public void ReceiveMessage<T>(string eventType, T genericType)
    {
        if(genericType is int)
        {
            rounds.text = "Round : " + genericType;
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHUD : MonoBehaviour,ICanRecieveMessages
{
    public static UIHUD instance;
    public List<UIObject> UI;
    public UIReload reload;

    /// <summary>
    /// cant seem to get it set on start correctly using this for now
    /// </summary>
    public ControllerPlayer player;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
           
        }
        else
        {
            Destroy(gameObject);
        }

        Initalize();
    }

    void Initalize()
    {
        EventSystem.SubscribeToEvent("HealthChanged", this);
       
        UpdateUI();
       // player.pawn.health.onHealthChanged += UpdateUI;
        
    }

    public void UpdateUI()
    {
        foreach (var  ui in UI)
        {
            ui.UpdateUI();
        }
    }

    public void ReceiveMessage<T>(string eventType, T genericType)
    {
        UpdateUI();
    }
}

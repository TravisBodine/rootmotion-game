﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class AudioSlider : MonoBehaviour
{
    
    [SerializeField] AudioMixerGroup mixerGroup;

    Slider slider;
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        slider.onValueChanged.AddListener(delegate { UpdateVolume(); });

        float audioMixerValue;
        mixerGroup.audioMixer.GetFloat(mixerGroup.name, out audioMixerValue);

        if(PlayerPrefs.HasKey("Audio " + mixerGroup.name))
        {
           
            slider.value = PlayerPrefs.GetFloat("Audio " + mixerGroup.name);
        }
        else
        {
            slider.value = Mathf.Pow(10, audioMixerValue);
        }
       
    

        //Debug.Log(Mathf.Pow(10, audioMixerValue));
    }

    public void UpdateVolume()
    {
 //       Debug.Log(20 * Mathf.Log10(slider.value));
        PlayerPrefs.SetFloat("Audio " + mixerGroup.name, slider.value);

        float vol = 20 * Mathf.Log10(slider.value);
        vol = Mathf.Clamp(vol, -80, 20);
        mixerGroup.audioMixer.SetFloat(mixerGroup.name,vol);

        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIReload : MonoBehaviour
{
    Image image;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        image.fillAmount = 0;
    }

    public void StartReloadCounter(float time)
    {
        StartCoroutine(ResizeReloadBar(time));
    }

    IEnumerator ResizeReloadBar(float totalTime)
    {
        float timer = 0;
        while(timer < totalTime)
        {
            image.fillAmount = timer / totalTime;
            timer += Time.deltaTime;

            yield return null;
        }

        image.fillAmount = 0;
    }
}

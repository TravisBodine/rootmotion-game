﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public ControllerPlayer player;
    //public ObjectPool<>
    public bool isPaused = false;
    public EnemySpawner enemySpawner;
    public List<ControllerAi> AliveEnemies = new List<ControllerAi>();
    public List<ControllerAi> DeadEnemies = new List<ControllerAi>();


    // Start is called before the first frame update
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void TogglePause(bool paused)
    {
        isPaused = paused;

        if (isPaused)
        {

            EventSystem.TriggerEvent("GamePaused", true);
            AudioManager.instance.Play("Pause");
            Time.timeScale = 0;
        }
        else
        {
            EventSystem.TriggerEvent("GamePaused", false);
            Time.timeScale = 1;
        }
    }

    
 
}

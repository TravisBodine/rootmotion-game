﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour, ICanRecieveMessages
{
    [SerializeField] GameObject spawnZonesHolder;
    [SerializeField] GameObject groundPlane;
    [SerializeField] ControllerAi enemy;

    [SerializeField] float spawnTime;

    [SerializeField] float timeBetweenRounds;
    [SerializeField] int roundsNeededToWin;

    [SerializeField] List<BoxCollider> SpawnZones;

    public int Round { get; private set; }

    public bool spawning = false;

    int AmountOfEnemiesToSpwan {
        get
        {
            return Round * 2;
        }
    }
    

    // Start is called before the first frame update
    void Start()
    {
        //at round 10 they win 
        //end of round when there are no more enemies left and its not still spawning them
        //at end of round delete all dead zombies// could pool but not enough time right now
        //remeber to add pausing and drops from enemies
        GameManager.instance.enemySpawner = this;
        EventSystem.SubscribeToEvent("EnemyDied", this);

        Round = 0;
        StartCoroutine(DelayBeforeNewRound(timeBetweenRounds));
    }


    //spawn on timer untill reached amount to spawn
    IEnumerator SpawnEnemys()
    {
        int enemiesSpawned = 0;
        float timer = 0;
        spawning = true;

        while (enemiesSpawned < AmountOfEnemiesToSpwan)
        {
      
            if(timer <= 0)
            {

                foreach (var spawnZone in SpawnZones)
                {

                    Vector3 pos = new Vector3(spawnZone.transform.position.x + Random.Range((spawnZone.center.x - spawnZone.bounds.extents.x), spawnZone.center.x + spawnZone.bounds.extents.x),
                        groundPlane.transform.position.y,
                        spawnZone.transform.position.z + Random.Range(spawnZone.center.z - spawnZone.bounds.extents.z, spawnZone.center.z + spawnZone.bounds.extents.z));

                    GameManager.instance.AliveEnemies.Add(Instantiate(enemy, pos, Quaternion.identity));


                    enemiesSpawned++;
                    if (enemiesSpawned >= AmountOfEnemiesToSpwan)
                    {
                        break;
                    }

                }

                timer = spawnTime;
            }

            if(!GameManager.instance.isPaused)
                timer -= Time.deltaTime;
            yield return null;
        }
        spawning = false;

    }

    void CheckForEndOfRound()
    {
      
        if((GameManager.instance.AliveEnemies.Count <= 0) && (spawning == false))
        {
            
            EndOfRound();
        }
    }

    void EndOfRound()
    {
        
        if (Round >= roundsNeededToWin)
        {
            //GameManager.instance.TogglePause(true);
            EventSystem.TriggerEvent("GameWon", Round);
            AudioManager.instance.Play("GameWin");
        }
        else
        {
            StartCoroutine(DelayBeforeNewRound(timeBetweenRounds));
        }
    }

    IEnumerator DelayBeforeNewRound(float time)
    {
        float timer = 0;
        while (timer < time)
        {
            if (!GameManager.instance.isPaused)
            {
                timer += Time.deltaTime;
            }
            yield return null;

        }
        CleanUpDeadEnemies();
        StartNewRound();
    }

    void CleanUpDeadEnemies()
    {
        GameManager.instance.AliveEnemies.Clear();

        foreach (var enemy in GameManager.instance.DeadEnemies)
        {
            Debug.Log(enemy);
            //should do like a fizzle or sink into the ground intead but dont have enough time
            Destroy(enemy.gameObject);
        }
        GameManager.instance.DeadEnemies.Clear();
    }

    void StartNewRound()
    {
        AudioManager.instance.Play("RoundStart");
        Round++;
        StartCoroutine(SpawnEnemys());
        EventSystem.TriggerEvent("RoundIncreased", Round);
    }

    private void OnDrawGizmosSelected()
    {
        if(SpawnZones != null)
        {
            Gizmos.color = Color.red;
            foreach (var spawnZone in SpawnZones)
            {
                Gizmos.DrawCube(spawnZone.transform.position, spawnZone.bounds.size);
            }
        }
  
    }

    public void ReceiveMessage<T>(string eventType, T genericType)
    {
        CheckForEndOfRound();
    }
    private void OnDestroy()
    {
        EventSystem.UnsubscribeToEvent("EnemyDied", this);
    }
}

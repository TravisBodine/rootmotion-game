﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct WeightedDrop
{
    public PowerUp drop;
    public float weight;
}

public class WeightedDropTable : MonoBehaviour
{
    public int noDropWeight;
    [SerializeField] List<WeightedDrop> drops;



    public PowerUp DropRandomItem()
    {
        //create list of cummalitive values for each item so we know what items corrispont to what total weight
        float total = noDropWeight;
        List<float> cummulativeValue = new List<float>();
        for (int i = 0; i < drops.Count; i++)
        {
            total += drops[i].weight;
            cummulativeValue.Add(total);
        }

        float randomValue = Random.Range(0, total);
        if(randomValue < noDropWeight)
        {
            return null;
        }
        else
        {
            total -= noDropWeight;
        }

        //binary search is faster 
        int selectedIndex = System.Array.BinarySearch(cummulativeValue.ToArray(), randomValue);
        if (selectedIndex < 0)
        {
            selectedIndex = ~selectedIndex;
        }
            
        return drops[selectedIndex].drop;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;



[System.Serializable]
public class AudioData
{
    public string name;
    public AudioClip clip;
    [Range(0,1)]
    public float volume;
    public bool playOnAwake;
    public AudioMixerGroup mixerGroup;
    [HideInInspector] public AudioSource source;

}


public class AudioManager : MonoBehaviour,ICanRecieveMessages
{
    [HideInInspector] public static AudioManager instance;

    [SerializeField] public AudioData[] sounds;

    Dictionary<string, AudioData> soundNames;
    [SerializeField] AudioMixer audioMixer;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        soundNames = new Dictionary<string, AudioData>();

        EventSystem.SubscribeToEvent("GamePaused", this);
        foreach (AudioData sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.playOnAwake = sound.playOnAwake;
            sound.source.outputAudioMixerGroup = sound.mixerGroup;
            soundNames.Add(sound.name, sound);
        }
    }

    public void Play(string nameOfClip)
    {
        soundNames[nameOfClip].source.Play();
    }
    public void Stop(string nameOfClip)
    {
        soundNames[nameOfClip].source.Stop();
    }

    public void ReceiveMessage<T>(string eventType, T genericType)
    {
        if (GameManager.instance.isPaused)
        {
            foreach (AudioData sound in sounds)
            {
                if (sound.source.isPlaying)
                {
                    sound.source.Pause();
                }
            }
        }
        else
        {
            foreach (AudioData sound in sounds)
            {
                sound.source.UnPause();
            }
        }
    }
}

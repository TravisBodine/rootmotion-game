﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform targetToFollow;

    public Vector3 offset = new Vector3(0,10,0);

    public float cameraSpeed= 0f;
    public float cameraDistanceInFront;
    [Range(0,1)]
    public float CameraLookSpeed;

    //removethis later

    public ControllerPlayer player;
    Camera cam;
    Transform tf;
    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(targetToFollow != null)
        {
            //make it go to mouse position unless it far enouch away
            Vector3 targetPos;



            targetPos = targetToFollow.position + (targetToFollow.transform.forward * cameraDistanceInFront);

            tf.position =
                targetToFollow.position +
                offset; // Vector3.MoveTowards(tf.position, targetPos + offset, cameraSpeed * Time.deltaTime);

            //TODO: somehow stop the camera from rotating weirdly when you go backwards
            //tf.rotation = Quaternion.Lerp(tf.rotation,Quaternion.LookRotation(targetToFollow.position - tf.position), CameraLookSpeed * Time.deltaTime);
        }
    }
}

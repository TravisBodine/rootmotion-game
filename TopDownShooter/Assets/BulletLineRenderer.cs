﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLineRenderer : MonoBehaviour
{
    public float fadeTime;
    public Material Material;

     LineRenderer lineRenderer;

    float timer;
    float initalWidth;
    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        //lineRenderer.material = Material;
        initalWidth = lineRenderer.startWidth;
        timer = fadeTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(lineRenderer.startColor.a > 0)
        {
            float amount = (timer / fadeTime) ;
            amount = Mathf.Clamp(amount, 0, 1);
            Color color = new Color (Material.color.r,Material.color.g,Material.color.b, amount);
            lineRenderer.startColor = color;
            lineRenderer.endColor = color;

            lineRenderer.startWidth = amount * initalWidth;

            timer -= Time.deltaTime;
        }
        else
        {
            //should probally pool these
            Destroy(gameObject);
        }
       
    }
}
